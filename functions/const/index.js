const POSTS = 'posts';
const USERS = 'users';

module.exports = {
    POSTS, USERS
};