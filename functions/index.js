require('dotenv').config();

const functions = require('firebase-functions');
const admin = require('firebase-admin');
const firebase = require('firebase');
const {POSTS, USERS} = require('./const');

const app = require('express')();

const firebaseConfig = {
    apiKey: "AIzaSyDWMt1k9d_GkYU-z4doRhgK9uzU9r2Q8TE",
    authDomain: "screamsocialapp.firebaseapp.com",
    databaseURL: "https://screamsocialapp.firebaseio.com",
    projectId: "screamsocialapp",
    storageBucket: "screamsocialapp.appspot.com",
    messagingSenderId: "1062389614065",
    appId: "1:1062389614065:web:efff23ce96caf7c2b93050",
    measurementId: "G-26BV35PWTZ"
};

// firebase serve
const serviceAccount = require(process.env.JSON_PATH.toString());
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});
firebase.initializeApp(firebaseConfig);

// firebase deploy
// admin.initializeApp();

const db = admin.firestore();

app.get('/getPosts', (req, res) => {
    db.collection(POSTS)
        .orderBy('createdAt', 'desc').get()
        .then(data => {
            let posts = [];
            data.forEach(doc => {
                posts.push({
                    postId: doc.id,
                    body: doc.data().body,
                    userHandle: doc.data().userHandle,
                    createdAt: doc.data().createdAt  
                });
            })
            return res.json(posts);
        })
        .catch(err => {
            console.error(err);
            res.status(500).json({error: "Can't get posts"});
        });
});

app.post('/createPost', (req, res) => {
    const newPost = {
        body: req.body.body,
        userHandle: req.body.userHandle,
        createdAt: new Date().toISOString()
    }
    
    db.collection(POSTS).add(newPost)
        .then(doc => {
            res.json({message: `Document ${doc.id} created successfully`})
        })
        .catch(err => {
            console.error(err);
            res.status(500).json({error: "Can't create post"});
        });
});

app.post('/signup', (req, res) => {
    const newUser = {
        email: req.body.email,
        password: req.body.password,
        confirmPassword: req.body.confirmPassword,
        handle: req.body.handle
    };

    //TODO validate req

    db.doc(`/${USERS}/${newUser.handle}`).get()
        .then(data => {
            //vraca datu i ako ima i ako nema pa mora provera
            if (data.exists) {
                res.status(400).json({handle: 'Handle is already taken'});
            } else {
                return firebase.auth()
                    .createUserWithEmailAndPassword(newUser.email, newUser.password);
            }
        })
        .then(data => {
            return data.user.getIdToken();
        })
        .then(token => {
            return res.status(201).json({token});
        })
        .catch(err => {
            console.error(err);
            if (err.code === 'auth/email-already-in-use')
                res.status(400).json({error: err.message});
            res.status(500).json({error: err.message});
        });
});

app.post('/signin', (req, res) => {
    firebase.auth().signInWithEmailAndPassword(req.body.email, req.body.password)
        .then(data => {
            return res.status(200)
                .json({message: `user ${data.user.uid} successfully logged in`})
        })
        .catch(err => {
            console.error(err);
            res.status(500).json({error: err.message});
        });
});

// exportovace sve funkcije koje se nalaze u app ali ce pre toga dodati /api
// e.g. https://baseurl/api/getPosts 
exports.api = functions.region('europe-west1').https.onRequest(app);